APIREST
=======

Generate a REST API with node.js and MongoDB







API Node Jitsi Temp Rooms!
===================

como instalar y usar las diferentes peticiones
----------


Instalacion
-------------

El los archivos de la api se encuentran dentro del repo en la siguente ruta: **cambios_jitsi/apiNodeJitsiFonax/**

cuando ya estemos ubicado en esa ruta procedemos a instalar las dependencia con npm

**npm install**

**npm install -g pm2 nodemon**

luego para ejecutar la api corremos el siguiente comando

 **node_modules/nodemon/bin/nodemon.js app.js localhost 8080**

ejecutar como un servicio
**pm2 start ~/apiNodeJitsiFonax/app.js -i 4 -- --port=8080**



Nginx
-------------
```
server {
 listen 8081 ssl;
 server_name 18.220.5.60;

 ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
 ssl_prefer_server_ciphers on;
 ssl_ciphers "EECDH+ECDSA+AESGCM:EECDH+aRSA+AESGCM:EECDH+ECDSA+SHA256:EECDH+aRSA+SHA256:EECDH+ECDSA+SHA384:EECDH+ECDSA+SHA256:EECDH+aRSA+SHA384:EDH+aRSA+AESGCM:EDH+aRSA+SHA256:EDH+aRSA:EECDH:!aNULL:!eNULL:!MEDIUM:!LOW:!3DES:!MD5:!EXP:!PSK:!SRP:!DSS:!RC4:!SEED";

 add_header Strict-Transport-Security "max-age=31536000";

 ssl_certificate /etc/jitsi/meet/18.220.5.60.crt;
 ssl_certificate_key /etc/jitsi/meet/18.220.5.60.key;

 root /usr/share/jitsi-meet;
 index index.html index.htm;
 error_page 404 /static/404.html;

# pass requests to port 8002 where our other node server is running
 location / {
  proxy_set_header X-Real-IP $remote_addr;
  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header X-NginX-Proxy true;
  proxy_pass http://localhost:8080;
  proxy_ssl_session_reuse off;
  proxy_set_header Host $http_host;
  proxy_cache_bypass $http_upgrade;
  proxy_redirect off;
  if ($request_method = 'OPTIONS') {
   add_header 'Access-Control-Allow-Origin' '*';
   add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
   add_header 'Access-Control-Max-Age' 1728000;
   add_header 'Content-Type' 'text/plain charset=UTF-8';
   add_header 'Content-Length' 0;
   return 204;
  }
  if ($request_method = 'POST') {
   add_header 'Access-Control-Allow-Origin' '*';
   add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
   add_header 'Access-Control-Allow-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range';
   add_header 'Access-Control-Expose-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range';
  }
  if ($request_method = 'GET') {
   add_header 'Access-Control-Allow-Origin' '*';
   add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
   add_header 'Access-Control-Allow-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range';
   add_header 'Access-Control-Expose-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range';
  }
 }
}
```


Uso
-------------

la api en su gran mayoria usa peticiones get y post

**Obtener todas los cuartos disponibles:**
 app.get('/rooms', findALLrooms);

 **Añadir un cuarto,  enviando el nombre del cuarto (se auto genera una url):**
 app.post('/rooms', addRoom);

  **Añadir un cuarto, enviando la url personalizada y el nombre del cuarto:**
 app.post('/rooms/add', addRoom2);

 **Actualizar la fecha actual del cuarto**
 app.post('/rooms/updateUrl', updateRoom);

  **Eliminar el cuarto por la url**
 app.post('/rooms/delete/', deleteRoom);



alguna de esas rutas estan implementadas en el siguiente archivo que se encuentra en el repositorio
**cambios_jitsi/room/libs/app.room.min.js**




