var express = require("express"),
 app = express(),
 bodyParser  = require("body-parser"),
 methodOverride = require("method-override");
mongoose = require('mongoose');


//connection DB
mongoose.connect('mongodb://localhost/rocketChat', function(err, res){
 if (err) console.log('ERROR:Connecting to DB:'+ err);
 else console.log('Connected');

});
var models = require('./rooms-esquema')(app, mongoose);


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

var router = express.Router();

router.get('/', function(req, res) {
   res.send(" API Working ");
});

app.use(router);


app.listen(8080, function() {
 console.log("Node server in  http://localhost:8080");
 routes =require ('./routes.js') (app);
});
