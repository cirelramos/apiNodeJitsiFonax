
module.exports = function (app) { // eslint-disable-line
	var RoomsSchema = require('./rooms-esquema'); // eslint-disable-line

  function makeUrl(req, res) {
    var text = '';
    var charLetters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < 5; i += 1) { text += charLetters.charAt(Math.floor(Math.random() * charLetters.length)); }

    return text;
  }


  // DELETE ROOM
  function deleteRoom(req, res) {
    var myquery = { url: req.body.url };
    RoomsSchema.remove(myquery, (err, obj) => {
      if (!err) console.log('Room Deleted!');
    });
  }

  // GET-return all rooms in the DB
  function findALLrooms(req, res) {
    RoomsSchema.find((err, rooms) => {
      if (!err) res.send(rooms);
      else console.log(`ERROR:${err}`);
      return 'none';
    });
  }

  // GET-Return a rom with a specified ID
  function findById(req, res) {
    RoomsSchema.findById(req.params.id, (err, room) => {
      if (!err) res.send(room);
      else console.log(`ERROR:${err}`);
    });
  }

  function findByUrl(req, res) {
    RoomsSchema.findOne({ url: req.params.url }, (err, room) => {
      if (!err) {
        console.log(room._id);
        res.send(room._id);
      } else console.log(`ERROR:${err}`);
    });
  }

  // POST-Insert a room DB
  function addRoom(req, res) {
    let urlGenerate = makeUrl();
    const minutesToAdjust = 1440;
    const originalDate = new Date().getTime();
    let room = new RoomsSchema({
      url: urlGenerate,
      name: req.body.name,
      date: new Date(),
      createdAt: new Date(),
      logEvent: 2,
      logMessage: 'Success!',
      expire_At: new Date(originalDate + (minutesToAdjust * 60000)),
    });
    // urlGenerate = {url:urlGenerate};
    res.send(room);
    room.save((err) => {
      if (!err) console.log('Room Create!');
      else console.log(`ERROR${err}`);
    });

    // res.send(room);
  }

  function addRoom2(req, res) {
    console.log('POST');
    console.log(req.body);
    const minutesToAdjust = 2;
    const millisecondsPerMinute = 60000;
    const originalDate = new Date();
    const modifiedDate1 = new Date(originalDate.valueOf() + minutesToAdjust);
    console.log(modifiedDate1);
    let room = new RoomsSchema({
      url: req.body.url,
      name: req.body.name,
      date: new Date(),
      createdAt: new Date(),
      logEvent: 2,
      logMessage: 'Success!',
      expire_At: modifiedDate1,
    });

    room.save((err) => {
      if (!err) console.log('Room Create!');
      else console.log(`ERROR${err}`);
    });

    res.send(room);
  }

  // PUT-update a room to DB
  function updateRoom(req, res) {
    console.log(req.body);
    console.log(`${req.body.url} --- ${req.body.date2}`);
    let newValues = { $set: { date2: req.body.date2 } };
    RoomsSchema.find({ url: req.body.url }, (err, room) => {
      if (!err) {
        console.log(room);
        console.log(room[0]._id);
        res.send(room._id);
        RoomsSchema.update({ _id: room[0]._id }, newValues, (err2, res2) => {
          if (!err2) {
            console.log('1 document updated');
          }
        });
      } else console.log(`ERROR:${err}`);
    });
  }


  // API ROUTES

  app.get('/rooms', findALLrooms);
  app.get('/rooms/:id', findById);
  app.get('/rooms/url/:url', findByUrl);
  app.post('/rooms', addRoom);
  app.post('/rooms/add', addRoom2);
  app.post('/rooms/updateUrl', updateRoom);
  app.post('/rooms/delete/', deleteRoom);
};
