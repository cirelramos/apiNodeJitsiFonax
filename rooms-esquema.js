var mongoose = require('mongoose');

Schema = mongoose.Schema;

let RoomsSchema = new Schema({
  url: { type: String },
  name: { type: String },
  date: { type: Date },
  createdAt: { type: Date },
  logEvent: { type: Number },
  logMessage: { type: String },
  expire_At: { type: Date },
  minTime: { type: Number },
  clasific: {
    type: String,
    enum: ['temp', 'perm']
  },
  resumen: { type: String }
});

module.exports = mongoose.model('room', RoomsSchema);
